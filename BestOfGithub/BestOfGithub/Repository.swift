//
//  Repository.swift
//  BestOfGithub
//
//  Created by Enrique Melgarejo on 18/01/17.
//  Copyright © 2017 Choynowski. All rights reserved.
//

import Foundation
import ObjectMapper
import CoreData

class Repository: NSManagedObject {
    
    static func repositories(for page: Int) -> [Repository] {
        let predicate = NSPredicate(format: "page == %d", page)
        let sortDescriptor = NSSortDescriptor(key: "starCount", ascending: false)
        let repositories: [Repository] = getEntities(withPredicate: predicate, sortDescriptor: sortDescriptor)
        return repositories
    }
    
    static func repository(with id: Int) -> Repository {
        let predicate = NSPredicate(format: "id == %d", id)
        let repositories: [Repository] = getEntities(withPredicate: predicate)
        if let repository = repositories.first {
            return repository
        } else {
            let newRepository: Repository = newEntity()
            newRepository.id = id as NSNumber?
            return newRepository
        }
    }
}

extension Repository: StaticMappable {
    
    static func objectForMapping(map: Map) -> BaseMappable? {
        guard let repositoryId: Int = map["id"].value() else { return nil }
        
        return Repository.repository(with: repositoryId)
    }
    
    func mapping(map: Map) {
        id              <- map["id"]
        forksCount      <- map["forks_count"]
        openIssuesCount <- map["open_issues_count"]
        starCount       <- map["stargazers_count"]
        watchersCount   <- map["watchers_count"]
        fullName        <- map["full_name"]
        language    	<- map["language"]
        name            <- map["name"]
        repDescription  <- map["description"]
        owner           <- map["owner"]
    }
}
