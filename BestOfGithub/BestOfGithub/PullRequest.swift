//
//  PullRequest.swift
//  BestOfGithub
//
//  Created by Enrique Melgarejo on 18/01/17.
//  Copyright © 2017 Choynowski. All rights reserved.
//

import Foundation
import ObjectMapper
import CoreData

class PullRequest: NSManagedObject {
    
    static func pullRequest(with id: Int) -> PullRequest {
        let predicate = NSPredicate(format: "id == %d", id)
        let pullRequests: [PullRequest] = getEntities(withPredicate: predicate)
        if let pullRequest = pullRequests.first {
            return pullRequest
        } else {
            let newPullRequest: PullRequest = newEntity()
            newPullRequest.id = id as NSNumber?
            return newPullRequest
        }
    }
}

extension PullRequest: StaticMappable {
    
    static func objectForMapping(map: Map) -> BaseMappable? {
        guard let pullRequestId: Int = map["id"].value() else { return nil }
        
        return PullRequest.pullRequest(with: pullRequestId)
    }
    
    func mapping(map: Map) {
        id      <- map["id"]
        number  <- map["number"]
        body    <- map["body"]
        state   <- map["state"]
        title   <- map["title"]
        url     <- map["html_url"]
        user    <- map["user"]
        createdAt <- (map["created_at"], Transforms.stringToDateTransform)
    }
}
