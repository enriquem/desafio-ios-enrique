//
//  RepositoryTableViewCell.swift
//  BestOfGithub
//
//  Created by Enrique Melgarejo on 17/01/17.
//  Copyright © 2017 Choynowski. All rights reserved.
//

import UIKit
import SDWebImage

class RepositoryTableViewCell: UITableViewCell {
    
    static let cellIdentifier = "RepositoryTableViewCell"
    static let cellHeight: CGFloat = 145.0
    
    @IBOutlet weak var imageViewUser: UIImageView!
    @IBOutlet weak var labelUserName: UILabel!
    @IBOutlet weak var labelRepositoryName: UILabel!
    @IBOutlet weak var labelRepositoryDescription: UILabel!
    
    @IBOutlet weak var viewContainerInfo: UIView!
    @IBOutlet weak var labelWatchers: UILabel!
    @IBOutlet weak var labelStars: UILabel!
    @IBOutlet weak var labelForks: UILabel!
    @IBOutlet weak var labelIssues: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.shadowOffset = CGSize(width: 0, height: 1)
        layer.shadowRadius = 1.0
        layer.shadowOpacity = 0.2
        
        setupForEmptyCell()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        setupForEmptyCell()
    }

    fileprivate func setupForEmptyCell() {
        imageViewUser.image = UIImage(named: "PersonIcon")
        labelUserName.text = ""
        labelRepositoryName.text = ""
        labelRepositoryDescription.text = ""
        
        labelWatchers.text = ""
        labelStars.text = ""
        labelForks.text = ""
        labelIssues.text = ""
    }
    
    func setup(with viewModel: RepositoryCellViewModel) {
        
        if let url = viewModel.avatarURL {
            imageViewUser.sd_setImage(with: url, placeholderImage: UIImage(named: "PersonIcon"))
        }

        labelUserName.text = viewModel.userName
        labelRepositoryName.text = viewModel.repositoryName
        labelRepositoryDescription.text = viewModel.repositoryDescription
        
        labelWatchers.text = viewModel.watchers
        labelStars.text = viewModel.stars
        labelForks.text = viewModel.forks
        labelIssues.text = viewModel.issues
    }
}
