//
//  Repository+CoreDataProperties.swift
//  BestOfGithub
//
//  Created by Enrique Melgarejo on 19/01/17.
//  Copyright © 2017 Choynowski. All rights reserved.
//

import Foundation
import CoreData

extension Repository {
    
    @NSManaged var id              : NSNumber?
    @NSManaged var forksCount      : NSNumber?
    @NSManaged var openIssuesCount : NSNumber?
    @NSManaged var starCount       : NSNumber?
    @NSManaged var watchersCount   : NSNumber?
    @NSManaged var fullName        : String?
    @NSManaged var language        : String?
    @NSManaged var name            : String?
    @NSManaged var repDescription  : String?
    @NSManaged var owner           : User?
    
    @NSManaged var page            : NSNumber?
    
    @NSManaged var pulls           : Set<PullRequest>?
    @NSManaged var forks           : Set<Fork>?
}
