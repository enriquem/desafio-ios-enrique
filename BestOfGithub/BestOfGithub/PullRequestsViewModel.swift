//
//  PullRequestsViewModel.swift
//  BestOfGithub
//
//  Created by Enrique Melgarejo on 18/01/17.
//  Copyright © 2017 Choynowski. All rights reserved.
//

import Foundation

final class PullRequestViewModel: NSObject {
    
    var onDataSourceChanged: (() -> Void)?
    
    var isFetchingData: Bool = false
    var repositoryModel: RepositoryCellViewModel?
    var dataSource: [PullRequestCellViewModel] = [] {
        didSet {
            onDataSourceChanged?()
        }
    }
    
    fileprivate var dataSourceProvider = DataSourceProvider()
    
    func viewModelDidLoad() {
        fetchNewDataSourceIfNeeded()
    }
    
    func fetchNewDataSourceIfNeeded() {
        guard let userName = repositoryModel?.userName,
            let repositoryName = repositoryModel?.repositoryName,
            !isFetchingData else {
                return
        }
        isFetchingData = true
        
        dataSourceProvider.getPullRequests(for: userName, repositoryName: repositoryName) { [weak self] (pullRequests, error) in
            if let pullRequests = pullRequests {
                
                // Append new pull requests
                self?.dataSource = pullRequests.map(PullRequestCellViewModel.init)
            } else {
                print(error ?? "")
            }
            self?.isFetchingData = false
        }
    }
    
    func open(url: URL?) {
        guard let url = url else { return }
        
        UIApplication.shared.openURL(url)
    }
}
