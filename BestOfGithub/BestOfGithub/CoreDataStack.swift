//
//  CoreDataStack.swift
//  BestOfGithub
//
//  Created by Enrique Melgarejo on 18/01/17.
//  Copyright © 2017 Choynowski. All rights reserved.
//

import Foundation
import CoreData

class CoreDataStack {
    
    static let sharedInstance = CoreDataStack()
    
    // MARK: - Core Data stack
    lazy var applicationDocumentsDirectory: URL = {
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1]
    }()
    
    lazy var coreDataURL: URL = {
        let url = self.applicationDocumentsDirectory.appendingPathComponent("besfofgithub.sqlite")
        return url
    }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        let modelURL = Bundle.main.url(forResource: "BestOfGithub", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.coreDataURL
        var failureReason = "There was an error creating or loading the application's saved data."
        
        let options = [
            NSSQLiteAnalyzeOption: true,
            NSMigratePersistentStoresAutomaticallyOption: true,
            NSInferMappingModelAutomaticallyOption: true,
            ]
        
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: options)
            debugPrint("Persistent store at \(url) was added.")
        } catch {
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject?
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject?
            dict[NSUnderlyingErrorKey] = error as AnyObject?
            let wrappedError = NSError(domain: "com.choynowski.BestOfGithub", code: 9999, userInfo: dict)
            debugPrint("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            
            if #available(iOS 9.0, *) {
                do {
                    try coordinator.destroyPersistentStore(at: url, ofType: NSSQLiteStoreType, options: options)
                    debugPrint("Persistent store at \(url) was destroyed.")
                } catch {
                    debugPrint("Unresolved error \(error)")
                }
            }
            
            abort()
        }
        
        return coordinator
    }()
    
    lazy var managedObjectContext: NSManagedObjectContext = {
        let coordinator = self.persistentStoreCoordinator
        var context = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        context.persistentStoreCoordinator = coordinator
        context.mergePolicy = NSMergePolicy(merge: .mergeByPropertyObjectTrumpMergePolicyType)
        return context
    }()
    
    // MARK: - Core Data Saving support
    func saveContext (completion: ((Bool) -> ())? = nil) {
        managedObjectContext.performAndWait { [weak self] in
            if self?.managedObjectContext.hasChanges ?? false {
                do {
                    try self?.managedObjectContext.save()
                    debugPrint("Context Saved")
                    completion?(true)
                } catch {
                    let nserror = error as NSError
                    debugPrint("Unresolved error \(nserror), \(nserror.userInfo)")
                    completion?(false)
                    abort()
                }
            } else {
                completion?(true)
            }
        }
    }
    
    func eraseCoreData(completion: ((Bool) -> ())? = nil) {
        
        for entity in managedObjectModel.entities {
            if let name = entity.name {
                debugPrint("Erasing \(name)")
                self.eraseEntity(name)
            }
        }
        completion?(true)
    }
    
    func eraseEntity(_ entity: String) {
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        fetchRequest.sortDescriptors = []
        do {
            let results = try managedObjectContext.fetch(fetchRequest)
            for managedObject in results
            {
                let managedObjectData:NSManagedObject = managedObject as! NSManagedObject
                managedObjectContext.delete(managedObjectData)
                try managedObjectContext.save()
            }
            
            debugPrint("Erased \(entity)")
        } catch let error as NSError {
            debugPrint("Detele all data in \(entity) error : \(error) \(error.userInfo)")
        }
    }
}
