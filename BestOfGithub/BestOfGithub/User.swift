//
//  User.swift
//  BestOfGithub
//
//  Created by Enrique Melgarejo on 18/01/17.
//  Copyright © 2017 Choynowski. All rights reserved.
//

import Foundation
import ObjectMapper
import CoreData

class User: NSManagedObject {
    
    static func user(with id: Int) -> User {
        let predicate = NSPredicate(format: "id == %d", id)
        let users: [User] = getEntities(withPredicate: predicate)
        if let user = users.first {
            return user
        } else {
            let newUser: User = newEntity()
            newUser.id = id as NSNumber?
            return newUser
        }
    }
}

extension User: StaticMappable {
    
    static func objectForMapping(map: Map) -> BaseMappable? {
        guard let userId: Int = map["id"].value() else { return nil }
        
        return User.user(with: userId)
    }
    
    func mapping(map: Map) {
        id        <- map["id"]
        avatarUrl <- map["avatar_url"]
        login     <- map["login"]
        url       <- map["url"]
    }
}
