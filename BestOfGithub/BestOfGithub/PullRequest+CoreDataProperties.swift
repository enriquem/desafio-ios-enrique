//
//  PullRequest+CoreDataProperties.swift
//  BestOfGithub
//
//  Created by Enrique Melgarejo on 19/01/17.
//  Copyright © 2017 Choynowski. All rights reserved.
//

import Foundation
import CoreData

extension PullRequest {
    
    @NSManaged var id         : NSNumber?
    @NSManaged var number     : NSNumber?
    @NSManaged var body       : String?
    @NSManaged var state      : String?
    @NSManaged var title      : String?
    @NSManaged var url        : String?
    @NSManaged var user       : User?
    @NSManaged var createdAt  : Date?
    @NSManaged var repository : Repository?
}
