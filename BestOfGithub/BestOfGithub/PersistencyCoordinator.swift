//
//  PersistencyCoordinator.swift
//  BestOfGithub
//
//  Created by Enrique Melgarejo on 1/19/17.
//  Copyright © 2017 Choynowski. All rights reserved.
//

import UIKit

class PersistencyCoordinator {
    
    // Get
    class func objectForKey(defaultName: String) -> Any? {
        let userDefaults = UserDefaults.standard
        return userDefaults.object(forKey: defaultName)
    }
    
    class func boolForKey(defaultName: String) -> Bool {
        let userDefaults = UserDefaults.standard
        return userDefaults.bool(forKey: defaultName)
    }
    
    // Set
    class func setObject(value: AnyObject?, forKey defaultName: String) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(value, forKey: defaultName)
    }
    
    class func setBool(value: Bool, forKey defaultName: String) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(value, forKey: defaultName)
    }
    
    // Remove
    class func removeObjectForKey(defaultName: String) {
        let userDefaults = UserDefaults.standard
        userDefaults.removeObject(forKey: defaultName)
    }
}
