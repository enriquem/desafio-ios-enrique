//
//  RepositoriesViewController.swift
//  BestOfGithub
//
//  Created by Enrique Melgarejo on 17/01/17.
//  Copyright © 2017 Choynowski. All rights reserved.
//

import UIKit

final class RepositoriesViewController: UIViewController {
    
    @IBOutlet weak var tableViewRepositories: UITableView!
    
    fileprivate var viewModel = RepositoriesViewModel()
    fileprivate var refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        setupTableView()
        setupRefreshControl()
        setupViewModel()
    }
    
    fileprivate func setupUI() {
        title = viewModel.title
    }
    
    fileprivate func setupTableView() {
        tableViewRepositories.register(UINib(nibName: LoadingTableViewCell.cellIdentifier, bundle:nil), forCellReuseIdentifier: LoadingTableViewCell.cellIdentifier)
        
        tableViewRepositories.register(UINib(nibName: RepositoryTableViewCell.cellIdentifier, bundle:nil), forCellReuseIdentifier: RepositoryTableViewCell.cellIdentifier)
        
        tableViewRepositories.contentInset = UIEdgeInsetsMake(5, 0, 5, 0)
        tableViewRepositories.delegate = self
        tableViewRepositories.dataSource = self
        tableViewRepositories.tableFooterView = UIView()
    }
    
    fileprivate func setupRefreshControl() {
        refreshControl.addTarget(self, action: #selector(refreshDataSource), for: .valueChanged)
        refreshControl.tintColor = .black
        tableViewRepositories.addSubview(refreshControl)
    }
    
    fileprivate func setupViewModel() {
        viewModel.onInsertIndexes = { [weak self] indexPaths in
            DispatchQueue.main.async {
                self?.tableViewRepositories.beginUpdates()
                self?.tableViewRepositories.insertRows(at: indexPaths, with: .automatic)
                self?.tableViewRepositories.endUpdates()
            }
        }
    }
    
    @objc func refreshDataSource() {
        refreshControl.beginRefreshing()
        tableViewRepositories.reloadData()
        refreshControl.endRefreshing()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let pullRequestsViewController = segue.destination as? PullRequestsViewController,
            let repositoryModel = sender as? RepositoryCellViewModel {
            pullRequestsViewController.repositoryModel = repositoryModel
        }
    }
}

extension RepositoriesViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Repositories + loading cell
        return viewModel.dataSource.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = UITableViewCell()
        if viewModel.dataSource.isEmpty || indexPath.row == viewModel.dataSource.count {
            cell = tableView.dequeueReusableCell(withIdentifier: LoadingTableViewCell.cellIdentifier, for: indexPath)
        } else {
            cell = tableView.dequeueReusableCell(withIdentifier: RepositoryTableViewCell.cellIdentifier, for: indexPath)
            if let repositoryCell = cell as? RepositoryTableViewCell {
                let model = viewModel.dataSource[indexPath.row]
                repositoryCell.setup(with: model)
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if viewModel.dataSource.isEmpty || indexPath.row == viewModel.dataSource.count {
            viewModel.fetchNewDataSourceIfNeeded()
        }
    }
}

extension RepositoriesViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return RepositoryTableViewCell.cellHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let model = viewModel.dataSource[indexPath.row]
        performSegue(withIdentifier: Segues.goToPullRequests.rawValue, sender: model)
    }
}
