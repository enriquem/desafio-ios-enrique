//
//  RepositoryCellViewModel.swift
//  BestOfGithub
//
//  Created by Enrique Melgarejo on 19/01/17.
//  Copyright © 2017 Choynowski. All rights reserved.
//

import UIKit

struct RepositoryCellViewModel {
    
    fileprivate let repository: Repository
    
    init(repository: Repository) {
        self.repository = repository
    }
    
    var userName: String {
        return repository.owner?.login ?? ""
    }
    
    var avatarURL: URL? {
        let avatarURL = repository.owner?.avatarUrl ?? ""
        let url = URL(string: avatarURL)
        return url
    }
    
    var repositoryName: String {
        return repository.name ?? ""
    }
    
    var repositoryDescription: String {
        return repository.repDescription ?? ""
    }
    
    var watchers: String {
        return repository.watchersCount?.stringValue ?? ""
    }
    
    var stars: String {
        return repository.starCount?.stringValue ?? ""
    }
    
    var forks: String {
        return repository.forksCount?.stringValue ?? ""
    }
    
    var issues: String {
        return repository.openIssuesCount?.stringValue ?? ""
    }
}
