//
//  CustomMappables.swift
//  BestOfGithub
//
//  Created by Enrique Melgarejo on 19/01/17.
//  Copyright © 2017 Choynowski. All rights reserved.
//

import Foundation
import ObjectMapper

struct SearchRepositoryResponse: Mappable {
    
    var repositories: [Repository]?
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        repositories <- map["items"]
    }
}

