//
//  PullRequestsViewController.swift
//  BestOfGithub
//
//  Created by Enrique Melgarejo on 18/01/17.
//  Copyright © 2017 Choynowski. All rights reserved.
//

import UIKit

class PullRequestsViewController: UIViewController {

    @IBOutlet weak var tableViewPullRequests: UITableView!
    
    var repositoryModel: RepositoryCellViewModel?
    fileprivate var viewModel = PullRequestViewModel()
    fileprivate var refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        setupTableView()
        setupRefreshControl()
        setupViewModel()
    }
    
    fileprivate func setupUI() {
        title = repositoryModel?.repositoryName
    }
    
    fileprivate func setupTableView() {
        tableViewPullRequests.register(UINib(nibName: LoadingTableViewCell.cellIdentifier, bundle:nil), forCellReuseIdentifier: LoadingTableViewCell.cellIdentifier)
        
        tableViewPullRequests.register(UINib(nibName: PullRequestTableViewCell.cellIdentifier, bundle:nil), forCellReuseIdentifier: PullRequestTableViewCell.cellIdentifier)
        
        tableViewPullRequests.contentInset = UIEdgeInsetsMake(5, 0, 5, 0)
        tableViewPullRequests.delegate = self
        tableViewPullRequests.dataSource = self
        tableViewPullRequests.tableFooterView = UIView()
    }

    
    fileprivate func setupRefreshControl() {
        refreshControl.addTarget(self, action: #selector(refreshDataSource), for: .valueChanged)
        refreshControl.tintColor = .black
        tableViewPullRequests.addSubview(refreshControl)
    }
    
    fileprivate func setupViewModel() {
        viewModel.repositoryModel = repositoryModel
        viewModel.onDataSourceChanged = { [weak self] in
            DispatchQueue.main.async {
                self?.tableViewPullRequests.reloadData()
                self?.refreshControl.endRefreshing()
            }
        }
        viewModel.viewModelDidLoad()
    }
    
    @objc func refreshDataSource() {
        viewModel.fetchNewDataSourceIfNeeded()
    }
}


extension PullRequestsViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.isFetchingData ? 1 : viewModel.dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = UITableViewCell()
        if viewModel.isFetchingData {
            cell = tableView.dequeueReusableCell(withIdentifier: LoadingTableViewCell.cellIdentifier, for: indexPath)
        } else {
            cell = tableView.dequeueReusableCell(withIdentifier: PullRequestTableViewCell.cellIdentifier, for: indexPath)
            if let pullRequestCell = cell as? PullRequestTableViewCell {
                let model = viewModel.dataSource[indexPath.row]
                pullRequestCell.setup(with: model)
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let title = viewModel.isFetchingData ? "Carregando Pull Requests..." : nil
        return title
    }
}

extension PullRequestsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return PullRequestTableViewCell.cellHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let model = viewModel.dataSource[indexPath.row]
        viewModel.open(url: model.url)
    }
}




