//
//  User+CoreDataProperties.swift
//  BestOfGithub
//
//  Created by Enrique Melgarejo on 19/01/17.
//  Copyright © 2017 Choynowski. All rights reserved.
//

import Foundation
import CoreData

extension User {
    
    @NSManaged var id               : NSNumber?
    @NSManaged var avatarUrl        : String?
    @NSManaged var login            : String?
    @NSManaged var url              : String?
    
    @NSManaged var ownerRepositories: Set<Repository>?
    @NSManaged var pulls            : Set<PullRequest>?
    @NSManaged var forks            : Set<Fork>?
}
