//
//  GitFork.swift
//  BestOfGithub
//
//  Created by Enrique Melgarejo on 19/01/17.
//  Copyright © 2017 Choynowski. All rights reserved.
//

import Foundation
import ObjectMapper
import CoreData

class Fork: NSManagedObject {
    
    static func fork(with id: Int) -> Fork {
        let predicate = NSPredicate(format: "id == %d", id)
        let forks: [Fork] = getEntities(withPredicate: predicate)
        if let fork = forks.first {
            return fork
        } else {
            let newFork: Fork = newEntity()
            newFork.id = id as NSNumber?
            return newFork
        }
    }
}

extension Fork: StaticMappable {
    
    static func objectForMapping(map: Map) -> BaseMappable? {
        guard let forkId: Int = map["id"].value() else { return nil }
        
        return Fork.fork(with: forkId)
    }
    
    func mapping(map: Map) {
        id   <- map["id"]
        name <- map["name"]
        user <- map["user"]
    }
}
