//
//  PullRequestTableViewCell.swift
//  BestOfGithub
//
//  Created by Enrique Melgarejo on 19/01/17.
//  Copyright © 2017 Choynowski. All rights reserved.
//

import UIKit

class PullRequestTableViewCell: UITableViewCell {
    
    static let cellIdentifier = "PullRequestTableViewCell"
    static let cellHeight: CGFloat = 145.0
    
    @IBOutlet weak var imageViewUser: UIImageView!
    @IBOutlet weak var labelUserName: UILabel!
    @IBOutlet weak var labelPullRequestName: UILabel!
    @IBOutlet weak var labelPullRequestDescription: UILabel!
    
    @IBOutlet weak var viewContainerInfo: UIView!
    @IBOutlet weak var labelNumber: UILabel!
    @IBOutlet weak var labelState: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.shadowOffset = CGSize(width: 0, height: 1)
        layer.shadowRadius = 1.0
        layer.shadowOpacity = 0.2
        
        setupForEmptyCell()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        setupForEmptyCell()
    }
    
    fileprivate func setupForEmptyCell() {
        imageViewUser.image = UIImage(named: "PersonIcon")
        labelUserName.text = ""
        labelPullRequestName.text = ""
        labelPullRequestDescription.text = ""
        
        labelNumber.text = ""
        labelState.text = ""
    }
    
    func setup(with viewModel: PullRequestCellViewModel) {
        
        if let url = viewModel.avatarURL {
            imageViewUser.sd_setImage(with: url, placeholderImage: UIImage(named: "PersonIcon"))
        }
        
        labelUserName.text = viewModel.userName
        labelPullRequestName.text = viewModel.title
        labelPullRequestDescription.text = viewModel.body
        
        labelNumber.text = viewModel.number
        labelState.text = viewModel.createdAt
    }
}
