//
//  PullRequestCellViewModel.swift
//  BestOfGithub
//
//  Created by Enrique Melgarejo on 19/01/17.
//  Copyright © 2017 Choynowski. All rights reserved.
//

import UIKit

struct PullRequestCellViewModel {
    
    fileprivate let pullRequest: PullRequest
    
    init(pullRequest: PullRequest) {
        self.pullRequest = pullRequest
    }
    
    var userName: String {
        return pullRequest.user?.login ?? ""
    }
    
    var avatarURL: URL? {
        let avatarURL = pullRequest.user?.avatarUrl ?? ""
        let url = URL(string: avatarURL)
        return url
    }
    
    var number: String? {
        return "#" + (pullRequest.number?.stringValue ?? "")
    }
    
    var body: String {
        return pullRequest.body ?? ""
    }
    
    var state: String {
        return pullRequest.state ?? ""
    }
    
    var createdAt: String {
        let createDate = pullRequest.createdAt?.createdAtDateFormat() ?? ""
        return "Created at: \(createDate)"
    }
    
    var title: String {
        return pullRequest.title ?? ""
    }
    
    var url: URL? {
        let pullUrl = pullRequest.url ?? ""
        let url = URL(string: pullUrl)
        return url
    }
}
