//
//  NSManagedObjectExtensions.swift
//  BestOfGithub
//
//  Created by Enrique Melgarejo on 19/01/17.
//  Copyright © 2017 Choynowski. All rights reserved.
//

import Foundation
import CoreData

extension NSManagedObject {
    
    class func entityName() -> String {
        return NSStringFromClass(self).components(separatedBy: ".").last!
    }
    
    class func entityDescription() -> NSEntityDescription {
        return NSEntityDescription.entity(forEntityName: self.entityName(), in: CoreDataStack.sharedInstance.managedObjectContext)!
    }
    
    class func newEntity<T: NSManagedObject>() -> T {
        return T(entity: entityDescription(), insertInto: CoreDataStack.sharedInstance.managedObjectContext)
    }
    
    class func getEntities<T: NSManagedObject>(withPredicate predicate: NSPredicate? = nil, sortDescriptor: NSSortDescriptor? = nil) -> [T] {
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        fetchRequest.predicate = predicate
        fetchRequest.entity = T.entityDescription()
        if let sortDescriptor = sortDescriptor {
            fetchRequest.sortDescriptors = [sortDescriptor]
        }
        
        do {
            let result = try CoreDataStack.sharedInstance.managedObjectContext.fetch(fetchRequest)
            if let objects = result as? [T] {
                return objects
            }
        } catch {
            return []
        }
        return []
    }
    
    class func getEntitiesAsync<T: NSManagedObject>(withPredicate predicate: NSPredicate? = nil, completion: @escaping ([T]) -> ()) {
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        fetchRequest.predicate = predicate
        fetchRequest.entity = T.entityDescription()
        
        let asyncFetch = NSAsynchronousFetchRequest(fetchRequest: fetchRequest) { result in
            if let result = result.finalResult as? [T] {
                completion(result)
            } else {
                completion([])
            }
        }
        
        do {
            try CoreDataStack.sharedInstance.managedObjectContext.execute(asyncFetch)
        } catch {
            completion([])
        }
    }
    
    func deleteEntity() {
        CoreDataStack.sharedInstance.managedObjectContext.delete(self)
        CoreDataStack.sharedInstance.saveContext()
    }
}


