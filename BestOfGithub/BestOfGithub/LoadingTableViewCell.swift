//
//  LoadingTableViewCell.swift
//  BestOfGithub
//
//  Created by Enrique Melgarejo on 1/19/17.
//  Copyright © 2017 Choynowski. All rights reserved.
//

import UIKit

class LoadingTableViewCell: UITableViewCell {
    
    static let cellIdentifier = "LoadingTableViewCell"
    static let cellHeight: CGFloat = 145.0
    
    @IBOutlet weak var activityIndicatorLoading: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        activityIndicatorLoading.startAnimating()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        activityIndicatorLoading.startAnimating()
    }
}
