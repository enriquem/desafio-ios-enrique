//
//  BGError.swift
//  BestOfGithub
//
//  Created by Enrique Melgarejo on 19/01/17.
//  Copyright © 2017 Choynowski. All rights reserved.
//

import Foundation

// See: https://developer.github.com/v3/#client-errors

public struct BGError {
    
    var errorMessage     : String = "Erro"
    var documentation_url: String = ""
    
    init() {}
    
    init(json: [String: AnyObject]) {
        self.errorMessage = json["message"] as? String ?? "Erro"
        self.documentation_url = json["documentation_url"] as? String ?? "Erro"
    }
    
    init(message: String) {
        self.errorMessage = message
    }
}
