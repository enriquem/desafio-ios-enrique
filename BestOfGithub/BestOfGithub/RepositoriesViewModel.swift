//
//  RepositoriesViewModel.swift
//  BestOfGithub
//
//  Created by Enrique Melgarejo on 18/01/17.
//  Copyright © 2017 Choynowski. All rights reserved.
//

import UIKit

final class RepositoriesViewModel: NSObject {
    
    var onInsertIndexes: (([IndexPath]) -> Void)?
    
    var title: String {
        return dataSourceProvider.selectedLanguage
    }
    var dataSource: [RepositoryCellViewModel] = []
    
    fileprivate var dataSourceProvider = DataSourceProvider()
    fileprivate var isFetchingData: Bool = false
    
    func fetchNewDataSourceIfNeeded() {
        guard !isFetchingData else { return }
        isFetchingData = true
        
        let lastFetchedPage = dataSourceProvider.lastFetchedRepositoryPage + 1
        dataSourceProvider.getRepositories(for: lastFetchedPage) { [weak self] (repositories, error) in
            if let repositories = repositories,
                let dataSource = self?.dataSource {
                
                let indexPaths = (dataSource.count..<(dataSource.count + repositories.count)).map({
                    return IndexPath(row: $0, section: 0)
                })
                
                // Append new repositories
                self?.dataSource += repositories.map(RepositoryCellViewModel.init)
                
                // Insert indexPaths
                self?.onInsertIndexes?(indexPaths)
            } else {
                print(error ?? "")
            }
            self?.isFetchingData = false
        }
    }
}
