//
//  ApplicationCoordinator.swift
//  BestOfGithub
//
//  Created by Enrique Melgarejo on 19/01/17.
//  Copyright © 2017 Choynowski. All rights reserved.
//

import Foundation

class ApplicationCoordinator: NSObject {
    static let shared = ApplicationCoordinator()
    
    var selectedLanguage = "Java"
}
