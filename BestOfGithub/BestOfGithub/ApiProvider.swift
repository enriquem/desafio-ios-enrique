//
//  ApiProvider.swift
//  BestOfGithub
//
//  Created by Enrique Melgarejo on 18/01/17.
//  Copyright © 2017 Choynowski. All rights reserved.
//

import Foundation
@_exported import Moya
@_exported import Result
@_exported import RxSwift

struct APIParameters {
    
    static let strBaseURL = "https://api.github.com"
    
    static let baseURL: URL = URL(string: strBaseURL)!
}

extension TargetType {
    
    var baseURL: URL {
        return APIParameters.baseURL
    }
}

private extension MoyaProvider {
    
    class func defaultJSONEndpointClosure(endpoint originalEndpoint: Target) -> Endpoint<Target> {
        let fullyFormedURL = originalEndpoint.baseURL.appendingPathComponent(originalEndpoint.path).absoluteString
        
        let endpoint = Endpoint<Target>(
            url: fullyFormedURL.removingPercentEncoding ?? fullyFormedURL,
            sampleResponseClosure: {.networkResponse(200, originalEndpoint.sampleData)},
            method: originalEndpoint.method,
            parameters: originalEndpoint.parameters,
            parameterEncoding: JSONEncoding()
        )
        
        return endpoint
    }
}

typealias APIProvider = RxMoyaProvider<BGEndpoint>

let networkActivityPlugin = NetworkActivityPlugin { state in
    switch state {
    case .began:
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        break
    case .ended:
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        break
    }
}

let BGApi = APIProvider(
    endpointClosure: MoyaProvider.defaultJSONEndpointClosure,
    plugins: [NetworkLoggerPlugin(verbose: false), networkActivityPlugin]
)
