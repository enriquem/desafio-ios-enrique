//
//  DataSourceProvider.swift
//  BestOfGithub
//
//  Created by Enrique Melgarejo on 1/19/17.
//  Copyright © 2017 Choynowski. All rights reserved.
//

import Foundation

enum APISort: String {
    case stars = "stars"
    case created = "created"
    case updated = "updated"
    case pushed = "pushed"
    case fullName = "full_name" // Default
}

typealias DatSourceResponse<T> = ((_ response: T?, _ error: BGError?) -> Void)?

class DataSourceProvider: NSObject {
    
    var selectedLanguage: String {
        return ApplicationCoordinator.shared.selectedLanguage
    }
    var lastFetchedRepositoryPage = 0
    
    func getRepositories(for page: Int, completion: DatSourceResponse<[Repository]>) {
       
       //TODO: Create offline repositories
//        let offlineRepositories = Repository.repositories(for: page)
//        
//        if offlineRepositories.count > 0 {
//            completion?(offlineRepositories, nil)
//        }
        
        let language = selectedLanguage
        let sort = APISort.stars.rawValue
        ApiRequests.searchRepositories(language, sort: sort, page: page) { [weak self] response in
            switch response {
            case .success(let repositories):
                // Save last fetched page
                self?.lastFetchedRepositoryPage = page
                
                // Save CoreData context
                CoreDataStack.sharedInstance.saveContext()
                
                completion?(repositories, nil)
            case .error(let error):
                print("Error Get Settings: \(error)")
                completion?(nil, error)
            }
        }
    }
    
    func getPullRequests(for ownerName: String, repositoryName: String, completion: DatSourceResponse<[PullRequest]>) {
        
        // TODO: Create offline pullRequests
        ApiRequests.pullRequests(of: ownerName, repositoryName: repositoryName) { response in
            switch response {
            case .success(let pullRequests):
                // Save CoreData context
                CoreDataStack.sharedInstance.saveContext()
                
                completion?(pullRequests, nil)
            case .error(let error):
                print("Error Get Settings: \(error)")
                completion?(nil, error)
            }
        }
    }
}




















