//
//  ApiRequests.swift
//  BestOfGithub
//
//  Created by Enrique Melgarejo on 19/01/17.
//  Copyright © 2017 Choynowski. All rights reserved.
//

import Foundation
import Moya
import Moya_ObjectMapper
import ObjectMapper

public enum NetworkResponse<T> {
    case success(response: T?)
    case error(error: BGError?)
}

private extension RxMoyaProvider {
    
    func requestJSON(_ token: Target) -> Observable<Any> {
        return request(token).filterSuccessfulStatusAndRedirectCodes().mapJSON()
    }
    
    func requestObject<T: BaseMappable>(_ token: Target) -> Observable<T> {
        return request(token).filterSuccessfulStatusAndRedirectCodes().mapObject(T.self)
    }
    
    func requestObjects<T: BaseMappable>(_ token: Target) -> Observable<[T]> {
        return request(token).filterSuccessfulStatusAndRedirectCodes().mapArray(T.self)
    }
}

struct ApiRequests {
    
    /**
     Search Repositories
     Find repositories via various criteria. This method returns up to 100 results per page.
     
     - language:    Searches repositories based on the language they're written in
     - sort:        The sort field. One of stars, forks, or updated. Default: results are sorted by best match.
     - page:        Specific pagination
     - completion:  A list of Repository objects. (Limited to 30 items by default)
     */
    static func searchRepositories(_ language: String, sort: String, page: Int, completion: @escaping (NetworkResponse<[Repository]>) -> ()) {
        _ = BGApi.requestObject(.searchRepositories(language: language, sort: sort, page: page)).subscribe(
            onNext: { (response: SearchRepositoryResponse) in
                completion(.success(response: response.repositories))
        },
            onError: { completion(.error(error: handleError($0))) }
        )
    }
    
    /**
     Pull Requests
     This request will return a list of pull requests of a given user and repository.
     
     - ownerName:       The user login.
     - repositoryName:  The repositoryName
     - completion:      A list of PullRequest objects.
     */
    static func pullRequests(of ownerName: String, repositoryName: String, completion: @escaping (NetworkResponse<[PullRequest]>) -> ()) {
        _ = BGApi.requestObjects(.pullRequests(ownerName: ownerName, repositoryName: repositoryName)).subscribe(
            onNext:  { completion(.success(response: $0 as [PullRequest])) },
            onError: { completion(.error(error: handleError($0))) }
        )
    }
    
    /**
     Forks
     This request will return a list of forks of a given user and repository.
     
     - ownerName:       The user login.
     - repositoryName:  The repositoryName
     - completion:      A list of Fork objects.
     */
    static func forks(of ownerName: String, repositoryName: String, completion: @escaping (NetworkResponse<[Fork]>) -> ()) {
        _ = BGApi.requestObjects(.forks(ownerName: ownerName, repositoryName: repositoryName)).subscribe(
            onNext:  { completion(.success(response: $0 as [Fork])) },
            onError: { completion(.error(error: handleError($0))) }
        )
    }
    
    // MARK: - Handle Error
    static func handleError(_ error: Swift.Error) -> BGError {
        if let e = error as? Moya.Error {
            if let bgError = e.parseForBGError() {
                return bgError
            } else {
                switch e {
                case .underlying(let nsError):
                    return BGError(message:(nsError as NSError).localizedDescription)
                default:
                    return BGError()
                }
            }
        } else {
            return BGError(message:(error as NSError).localizedDescription)
        }
    }
}

private extension Moya.Error {
    
    func parseForBGError() -> BGError? {
        guard let json = try? self.response?.mapJSON() as? [String: AnyObject],
            let jjson = json else {
                return nil
        }
        
        let bgError = BGError(json: jjson)
        return bgError
    }
}
