//
//  Fork+CoreDataProperties.swift
//  BestOfGithub
//
//  Created by Enrique Melgarejo on 19/01/17.
//  Copyright © 2017 Choynowski. All rights reserved.
//

import Foundation

extension Fork {
    
    @NSManaged var id  : NSNumber?
    @NSManaged var name: String?
    @NSManaged var user: User?
    
    @NSManaged var repository: Repository?
}
