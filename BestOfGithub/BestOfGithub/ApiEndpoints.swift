//
//  ApiEndpoints.swift
//  BestOfGithub
//
//  Created by Enrique Melgarejo on 18/01/17.
//  Copyright © 2017 Choynowski. All rights reserved.
//

import Foundation
import Moya

typealias Parameters = [String: Any]

enum BGEndpoint {
    // /search/repositories?q=language:{language}&sort={sort}&page={page}   [GET]
    case searchRepositories(language: String, sort: String, page: Int)
    // /repos/{ownerName}/{repositoryName}/pulls                            [GET]
    case pullRequests(ownerName: String, repositoryName: String)
    // /repos/{ownerName}/{repositoryName}/forks                            [GET]
    case forks(ownerName: String, repositoryName: String)
}

extension BGEndpoint: TargetType {
    var path: String {
        switch self {
        case .searchRepositories(let language, let sort, let page): return "search/repositories?q=language:\(language)&sort=\(sort)&page=\(page)"
        case .pullRequests(let ownerName, let repositoryName):      return "repos/\(ownerName)/\(repositoryName)/pulls"
        case .forks(let ownerName, let repositoryName):             return "repos/\(ownerName)/\(repositoryName)/pulls"
        }
    }
    
    var multipartBody: [MultipartFormData]? {
        return nil
    }
    
    var method: Moya.Method {
        switch self {
        case .searchRepositories:   return .get
        case .pullRequests:         return .get
        case .forks:                return .get
        }
    }
    
    var parameters: Parameters? {
        switch self {
        default:
            return nil
        }
    }
    
    public var parameterEncoding: ParameterEncoding {
        return JSONEncoding()
    }
    
    public var task: Moya.Task {
        return .request
    }
    
    var sampleData: Data {
        return Data()
    }
}
